# Disclaimer

The links below point to external repositories with games created by the community around KYPO Cyber Range Platform and Cyber Sandbox Creator.

Masaryk University provides no warranty and no guarantees regarding the linked content, including but not limited to its quality, correctness, safety of use, and completeness of documentation. The user is responsible for due diligence. Contact the authors of the individual games for further questions.

## Games created by the community

These projects were created by students and instructors of [Slovak University of Technology in Bratislava](https://www.stuba.sk/english.html?page_id=132) as part of educational projects, such as master theses.

* [Unpleasant](https://gitlab.fiit.stuba.sk/xjely/unpleasant)
* [DDoS Sandbox](https://gitlab.fiit.stuba.sk/xsvab/DDOS_sandbox)
* [gcomp-rescue](https://gitlab.fiit.stuba.sk/xbenov/gcomp-rescue)
* [purple-team](https://gitlab.fiit.stuba.sk/xlenickyj/purple-team)
* [DNS rebinding](https://gitlab.fiit.stuba.sk/xjancovicb/bp-DNS_Rebinding)
* [Remote cache poisoning](https://gitlab.fiit.stuba.sk/xjancovicb/bp-Remote_cache_poisoning)
* [Man in the middle](https://gitlab.fiit.stuba.sk/xblahovic/man-in-the-middle)
